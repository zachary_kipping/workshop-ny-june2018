import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Video } from '../../types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {
  @Input() videos: Video[];
  @Input() selected: Video;

  @Output() clickedVideo = new EventEmitter<Video>();

  constructor() { }

  ngOnInit() { }

  select(v: Video) {
    this.clickedVideo.emit(v);
  }
}
