import { Component, Input } from '@angular/core';
import { View } from '../../types';

@Component({
  selector: 'app-stat-display',
  templateUrl: './stat-display.component.html',
  styleUrls: ['./stat-display.component.css']
})
export class StatDisplayComponent {
  @Input() views: View[];
}
