import { Component, OnInit } from '@angular/core';

import { API_URL } from '../../constants';
import { Video, Filters, View } from '../../types';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent implements OnInit {
  videoList: Observable<Video[]>;
  selectedVideo: Video;
  viewList: View[] = [];
  filter: Filters = { minAge: 0, maxAge: 100 };

  constructor(http: HttpClient) {
    this.videoList = http.get<Video[]>(API_URL);
  }

  ngOnInit() { }

  selectVideo(v: Video) {
    this.selectedVideo = v;
    this.filterViews();
  }

  setFilter(f: Filters) {
    this.filter = f;
    this.filterViews();
  }

  filterViews() {
    if (this.selectedVideo) {
      this.viewList = this.selectedVideo.viewDetails.filter(view => {
        return (view.age >= this.filter.minAge && view.age <= this.filter.maxAge);
      });
    } else {
      this.viewList = [];
    }
  }
}
