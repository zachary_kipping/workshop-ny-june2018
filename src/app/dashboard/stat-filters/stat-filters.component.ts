import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Filters } from '../../types';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.css']
})
export class StatFiltersComponent implements OnInit {
  @Output() filtersChanged = new EventEmitter<Filters>();
  filterDetails: FormGroup;

  constructor(fb: FormBuilder) {
    this.filterDetails = fb.group({
      minAge: [0, Validators.required],
      maxAge: [100, Validators.required]
    });

    // event from template
    this.filterDetails.valueChanges.subscribe(changes => {
      // sending event to parent component
      this.filtersChanged.emit(changes);
    });

  }

  ngOnInit() { }
}
